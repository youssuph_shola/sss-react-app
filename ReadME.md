
DEPLOYMENT INSTRUCTIONS

1). Extract SSS-React-App into a folder. I am assuming the name of the folder is SSS-React-App.

2). Go to command line and CD (change directory) into SSS-React-App.

3). run $ npm install.

4). After step 3 is complete, run $ npm start.

5). Then visit http://localhost:8080



