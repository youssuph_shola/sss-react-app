import * as actionTypes from './action-types'

const initialState = {
  cartItems: {}, // array Each unique Item in cart
  items_count: 0, // number Total count of items (count(items)) in cart
  items_quantity: [], // array Quantity of Each Number of Items
}

export default function reducer (state = initialState, { type, payload }) {
  switch (type) {
    case actionTypes.CART_INIT:
      return initCart(state, payload)
    case actionTypes.CART_ADD_ITEM:
      return addItem(state, payload)
    case actionTypes.CART_UPDATE_ITEM:
      return updateItem(state, payload)
    case actionTypes.CART_REMOVE_ITEM:
      return removeItem(state, payload)
    default:
      return state
  }
}

function initCart (state, payload) {
  return {
    ...state,
    ...payload
  }
}

function addItem (state, payload) {
  return {
    ...state,
    items: [...state.items, ...payload.items],
    items_count: payload.items_count,
    items_quantity: [...state.items_quantity, ...payload.items_quantity],
  }
}

function updateItem(state, payload) {

  return {
    ...state,
    ...payload
  }
}

function removeItem (state, item_id) {
	
  return {
	  ...state,
	  items: payload.items,
	  items_count : payload.items_count,
	  items_quantity: payload.items_quantity
  }
  /*
  return {
    ...state,
    items: state.items.filter(item => item.item_id !== item_id),
    items_count: state.items_count - 1,
    items_quantity: state.items_quantity - 1,
  } */
}
