import * as actionTypes from './action-types'

export function init (payload) {
  return {
    type: actionTypes.CART_INIT,
    payload
  }
}

export function addItem (payload) {
  return {
    type: actionTypes.CART_ADD_ITEM,
    payload,
  }
}

export function updateItem(payload) {

  return {
    type: actionTypes.CART_UPDATE_ITEM,
    payload,
  }
}

export function removeItem (payload) {
  return {
    type: actionTypes.CART_REMOVE_ITEM,
    payload
  }
}
