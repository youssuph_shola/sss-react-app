import * as actionTypes from './action-types'

export function initGallery (payload) {
  return {
    type: actionTypes.GALLERY_INIT,
    payload
  }
}

export function setMainImage (payload) {
  return {
    type: actionTypes.SET_MAIN_IMAGE,
    payload,
  }
}


