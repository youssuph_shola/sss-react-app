import * as actionTypes from './action-types'

const initialState = {
  mainImage:0
}

export default function reducer (state = initialState, { type, payload }) {
  switch (type) {
    case actionTypes.GALLERY_INIT:
      return initGallery(state, payload)
    case actionTypes.SET_MAIN_IMAGE:
      return setMainImage(state, payload)
    default:
      return state
  }
}

function initGallery (state, payload) {
  console.log('initing gallery')
  return {
    ...state,
    ...payload
  }
}

function setMainImage (state, payload) {
  return {
    ...state,
    mainImage:payload
  }
}