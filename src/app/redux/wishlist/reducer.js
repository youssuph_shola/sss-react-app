import * as actionTypes from './action-types'

const initialState = {
  items: {},
  isInWishlist: false
}

export default function reducer (state = initialState, { type, payload }) {
  switch (type) {
    case actionTypes.WISHLIST_INIT:
      return initWishlist(state, payload)
    case actionTypes.WISHLIST_UPDATE_ITEM:
      return updateItem(state, payload)
    case actionTypes.SET_IS_ADDED:
      return setIsAdded(state, payload)      
    default:
      return state
  }
}

function initWishlist (state, payload) {
  return {
    ...state,
    ...payload
  }
}

function updateItem(state, payload) {

  return {
    ...state,
    ...payload
  }
}


function setIsAdded(state, payload) {

  return {
    ...state,
    isInWishlist : payload
  }
}