import * as actionTypes from './action-types'

export function init (payload) {
  return {
    type: actionTypes.WISHLIST_INIT,
    payload
  }
}

export function updateItem(payload) {

  return {
    type: actionTypes.WISHLIST_UPDATE_ITEM,
    payload,
  }
}

export function setIsAdded(payload) {

  return {
    type: actionTypes.SET_IS_ADDED,
    payload,
  }
}