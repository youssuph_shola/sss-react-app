import * as actionTypes from './action-types'

export function initProducts (payload) {
  return {
    type: actionTypes.PRODUCTS_INIT,
    payload
  }
}

export function setProducts (payload) {
  return {
    type: actionTypes.SET_PRODUCTS,
    payload,
  }
}

