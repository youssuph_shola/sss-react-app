import * as actionTypes from './action-types'

const initialState = {
  productItems : []
}

export default function reducer (state = initialState, { type, payload }) {
  switch (type) {
    case actionTypes.PRODUCTS_INIT:
      return initProducts(state, payload)
    case actionTypes.SET_PRODUCTS:
      return setProducts(state, payload)
    default:
      return state
  }
}

function initProducts (state, payload) {
  return {
    ...state,
    ...payload
  }
}

function setProducts (state, payload) {
  return {
    ...state,
    productItems: payload
  }
}