import { combineReducers } from 'redux'
import cartReducer from './cart'
import galleryReducer from './gallery'
import formstatus from './formstatus'
import products from './products'
import wishlist from './wishlist'

export default combineReducers({
  cart : cartReducer,
  gallery : galleryReducer,
  formstatus: formstatus,
  products: products,
  wishlist: wishlist
})
