/**
 * Main store function
 */
import { applyMiddleware, compose, createStore } from 'redux'
import thunk from 'redux-thunk'
import rootReducer from './reducer'

export default function (initialState = {}) {
  const enhancers = [
    applyMiddleware(thunk),
  ]

  return createStore(rootReducer, initialState, compose(...enhancers))
}
