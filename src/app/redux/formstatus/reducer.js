import * as actionTypes from './action-types'

const initialState = {
  color: null,
  size: null,
  quantity: null,
  cartButtonDisabled: true
}

export default function reducer (state = initialState, { type, payload }) {
  switch (type) {
    case actionTypes.SET_COLOR:
      return setColor(state, payload)
    case actionTypes.SET_SIZE:
      return setSize(state, payload)
    case actionTypes.SET_QUANTITY:
      return setQuantity(state, payload)      
    case actionTypes.SET_BUTTON_DISABLED:
      return setButtonDisabled(state, payload)       
    default:
      return state
  }
}

function setColor (state, payload) {
  return {
    ...state,
       color: payload    
  }
}

function setSize (state, payload) {
  return {
    ...state,
    size: payload
  }
}

function setQuantity (state, payload) {
  return {
    ...state,
    quantity: payload
  }
}

function setButtonDisabled(state, payload) {
  return {
    ...state,
    cartButtonDisabled: payload
  }
}