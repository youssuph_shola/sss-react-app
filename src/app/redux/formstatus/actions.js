import * as actionTypes from './action-types'

export function setColor (payload) {
  return {
    type: actionTypes.SET_COLOR,
    payload
  }
}

export function setSize (payload) {
  return {
    type: actionTypes.SET_SIZE,
    payload,
  }
}

export function setQuantity (payload) {
  return {
    type: actionTypes.SET_QUANTITY,
    payload,
  }
}

export function setButtonDisabled (payload) {
  return {
    type: actionTypes.SET_BUTTON_DISABLED,
    payload,
  }
}