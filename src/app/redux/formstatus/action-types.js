export const SET_COLOR = 'SET_COLOR'
export const SET_SIZE = 'SET_SIZE'
export const SET_QUANTITY = 'SET_QUANTITY'
export const SET_BUTTON_DISABLED = 'SET_BUTTON_DISABLED'
