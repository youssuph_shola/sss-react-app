import React from "react";
import { render } from "react-dom";
import {assetBaseUrl} from "../Constants/AssetConstants";


export class SearchHeader extends React.Component {
	
	render(){
		return (
			  <div id="search">
			      <img src={`${assetBaseUrl}search.jpg`} alt="" width="62" height="16" />
			  </div>
		);
	}
}