import { connect } from 'react-redux'
import MainGalleryImage from './MainGalleryImage'

function mapStateToProps (state) {
  return {
    gallery: state.gallery,
  }
}

export default connect(mapStateToProps)(MainGalleryImage)
