import React from "react";
import Swipeable from "react-swipeable";
import GalleryServices from "../../Services/GalleryServices";


const IMG_WIDTH = "auto";
const IMG_HEIGHT = "500px";

const RIGHT = '-1';
const LEFT = '+1';

export default class MainGalleryImage extends React.Component {
  constructor(props, context) {
    super(props, context);
    this.largeImages = GalleryServices.getLargeImages();
    this.preload.apply(null, this.largeImages);
    
  }

  onSwiped(direction) {
    let change = direction === RIGHT ? RIGHT : LEFT;

    const adjustedIdx = parseInt(this.props.gallery.mainImage) + Number(change);

    let newIdx;
    if (adjustedIdx >= this.largeImages.length) {
      newIdx = 0;
    } else if (adjustedIdx < 0) {
      newIdx = this.largeImages.length - 1
    } else {
      newIdx = adjustedIdx;
    }

   this.props.dispatch(GalleryServices.displayLargeImage(newIdx))
  }

  preload(...images){

    return images.reduce((acc, img)=>{
	    let newImage = new Image();
	    newImage.src = img;
	    acc.push(newImage);
	    return acc;
	}, []);	
  }

  render() {
    const { mainImage } = this.props.gallery;
    const imageStyles = {
      backgroundImage: `url(${this.largeImages[mainImage]})`,
    };

    return (
      <div className="swipeContainer">

        <Swipeable 
          className="swipe"
          trackMouse
          style={{ touchAction: 'none' }}
          preventDefaultTouchmoveEvent
          onSwipedLeft={()=>this.onSwiped(LEFT)}
          onSwipedRight={()=>this.onSwiped(RIGHT)}
        >
          <div  className='gallery-image'>
          <img className="LargeImage" src={`${this.largeImages[mainImage]}`} />
          </div>
        </Swipeable>
      </div>
    )
  }
}