import React from "react";

export default class MainGalleryImage extends React.Component {
	
	render(){
		return (
				<div id="mainimage">
				    <img className="LargeImage" src={this.props.gallery.mainImage} />
				</div>
		       );
	}
}