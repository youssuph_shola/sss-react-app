//Header.js
import React from "react";

export class Button extends React.Component{
	
	

	render(){

		return (
				    <input name={this.props.buttonName} 
				           type="button" 
				           className={this.props.className} 
				           id={this.props.buttonId}
				           value={this.props.buttonLabel}
				           disabled={this.props.isDisabled} 
				           onClick = {this.props.onClick} />
				
		          );
	}
}