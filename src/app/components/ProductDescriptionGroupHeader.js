//Header.js
import React from "react";
import { ProductDescriptionHeader } from "./ProductDescriptionHeader";
import { ProductDescriptionRating } from "./ProductDescriptionRating";
import { ProductDescriptionPrice } from "./ProductDescriptionPrice";

export class ProductDescriptionGroupHeader extends React.Component{
	
	render(){
		return (
			<div className = "desctextheader">
			    <ProductDescriptionHeader description={"The Atelier Tailored Coat"} />
			    <ProductDescriptionRating />
			    <ProductDescriptionPrice price="499.00" />
			</div>
		);
	}
}