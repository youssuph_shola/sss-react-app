import React from "react";
import { render } from "react-dom";
import {assetBaseUrl} from "../Constants/AssetConstants";


export class SSSLogo extends React.Component {
	
	render(){
		return (
			<div id="logodiv">
				<img src={`${assetBaseUrl}/logo.png`} width="204" height="40" />
			</div>
		);
	}
}