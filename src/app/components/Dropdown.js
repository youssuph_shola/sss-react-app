//Header.js
import React from "react";

export class Dropdown extends React.Component{
	
	

	render(){
		return (<div className="descsub" >
		          <div>{this.props.fieldLabel}</div>

		            <select name={this.props.fieldName} 
		                    className={this.props.fieldClassName}
		                    onChange={this.props.onChange}>
		              <option value='0'>{this.props.selectInstruction}</option>
		              {this.props.items && 
						this.props.items.map((item, i) => (
							<option key={i} value={item.value}>
								{item.label}
							</option>))
					  }
		            </select>
		    
		          <p>&nbsp;</p>
		        </div>);
	}
}