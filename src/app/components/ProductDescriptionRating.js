//Header.js
import React from "react";

export class ProductDescriptionRating extends React.Component{
	
	render(){
		return (

			<div className='clear'>
				<div className = 'rating-box'>
					<span className="fa fa-star fa-7x checked"></span>
					<span className="fa fa-star fa-xs checked"></span>
					<span className="fa fa-star fa-xs checked"></span>
					<span className="fa fa-star fa-xs checked"></span>
					<span className="fa fa-star fa-xs"></span>
				</div>
				<div className='review-count-text'>3 Review(s)   |</div>
				<div className='add-a-review'>ADD A REVIEW</div>
				<div className='clear'></div>
			</div>
		);
	}
}
