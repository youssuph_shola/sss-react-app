//Header.js
import React from "react";
import AddToCartButton from "./AddToCartButton";
import AddToWishlistButton from "./AddToWishlistButton";



export class DescriptionSubContainerButtons extends React.Component{

	render(){

		return (
			<div>
                <AddToCartButton />
                <AddToWishlistButton />
            </div>
		);
	}
}