import React from "react";
import { render } from "react-dom";

import { SideMobileMenu } from "./SideMobileMenu";
import { AppMenu } from "./AppMenu";
import { SearchHeader } from "./SearchHeader";


export class NavigationBar extends React.Component {
	
	render(){
		return (
			<div id="navibar">
				<SideMobileMenu />
				<AppMenu />
				<SearchHeader />
			</div>
		);
	}
}