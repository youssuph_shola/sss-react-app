//Header.js
import React from "react";
import { DescriptionSubContainerOne } from "./DescriptionSubContainerOne";
import { DescriptionSubContainerTwo } from "./DescriptionSubContainerTwo";
import { DescriptionSubContainerButtons } from "./DescriptionSubContainerButtons";
import DescriptionSubContainerForm  from "./DescriptionSubContainerForm";

export class DescriptionContainer extends React.Component{
	
	render(){
		return (
			<div id="descriptiondiv">
				<a href="#">&lt; Back</a>
			    <DescriptionSubContainerOne />
			    <DescriptionSubContainerTwo />
			    <DescriptionSubContainerForm />
			    <DescriptionSubContainerButtons />
			</div>
		);
	}
}