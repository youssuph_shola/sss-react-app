import { connect } from 'react-redux'
import DescriptionSubContainerForm from './DescriptionSubContainerForm'

function mapStateToProps (state) {
  return {
    formstatus: state.formstatus,
  }
}

export default connect(mapStateToProps)(DescriptionSubContainerForm)
