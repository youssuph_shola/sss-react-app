//Header.js
import React from "react";
import { Dropdown } from "../Dropdown";
import formstatusServices from '../../Services/FormstatusServices';
import WishlistServices from '../../Services/WishlistServices';



export default class DescriptionSubContainerForm extends React.Component{

	componentDidMount(){

		this.state={
			color:null,
			quantity: null,
			size: null,
			lastAlteredComponent : null
		}
	}

    getColorItems(){
        return [{value:'Red', label: 'Red'},
                {value:'Blue', label: 'Blue'},
                {value:'Green', label: 'Green'}];
    }

    getSizeItems(){
        return [{value:'S', label: 'S'},
                {value:'M', label: 'M'},
                {value:'L', label: 'L'},
                {value:'XL', label: 'XL'}]
    }


    getQuantityItems(){
        return [{value:1, label: 1},
                {value:2, label: 2},
                {value:3, label: 3},
                {value:4, label: 4},
                {value:5, label: 5},
                {value:6, label: 6},
                {value:7, label: 7},]
    }

    processColorChange=(event)=>{

    	let v = event.currentTarget.value;
    	this.setState({color: v, lastAlteredComponent: 'color'});
    	this.props.dispatch(formstatusServices.setColor(v));
    	this.processFormContentChange('color');
    }

    processSizeChange(event){

    	let v = event.currentTarget.value;
    	this.setState({size: v, lastAlteredComponent:'size'});
     	this.props.dispatch(formstatusServices.setSize(v));
     	this.processFormContentChange('size');
    }

    processQuantityChange(event){

    	let v = event.currentTarget.value;
    	this.setState({quantity: v, lastAlteredComponent:'quantity'});
    	this.props.dispatch(formstatusServices.setQuantity(v));
    	this.processFormContentChange(null);
    }

    processFormContentChange(alteredComponent){

    	let v = this.getFormValues();
    	this.props.dispatch(formstatusServices.setAddToCartButtonStatus(v));
    	formstatusServices.resetFields(alteredComponent, v, 
    									{color: this.getColorField(), 
    									 size: this.getSizeField()});

    	WishlistServices.updateWishlistStatus({color:v.color, 
    										   size:v.size});

    }


    getFormValues(){
    	let color = this.getColorField().value,
    		size = this.getSizeField().value,
    		quantity = this.getQuantityField().value; 
    	
    	/*let color = this.state.color,
    		size = this.state.size,
    		quantity = this.state.quantity;*/

    	return {color, size, quantity};
    }

    getColorField(){
		return document.getElementsByName('colorField')[0];
    }

    getSizeField(){
		return document.getElementsByName('sizeField')[0];
    }

    getQuantityField(){
		return document.getElementsByName('quantityField')[0];
    }

	render(){

		return (<div className='form-container'>
    			    <Dropdown fieldLabel="COLOR"
                                 fieldName="colorField"
                                 fieldClassName = ""
                                 selectInstruction = "Select Color"
                                 items = {this.getColorItems()}
                                 onChange = {(event)=>{this.processColorChange(event)}} />

                    <Dropdown fieldLabel="SIZE"
                                 fieldName="sizeField"
                                 fieldClassName = ""
                                 selectInstruction = "Select Size"
                                 items = {this.getSizeItems()}
                                  onChange = {(event)=>{this.processSizeChange(event)}} />


                    <Dropdown fieldLabel="QUANTITY"
                                 fieldName="quantityField"
                                 fieldClassName = ""
                                 selectInstruction = "Select Quantity"
                                 items = {this.getQuantityItems()}
                                 onChange = {(event)=>{this.processQuantityChange(event)}} />

                    <div className="clear"></div>
                </div>
		       );
	}
}