import React from "react";
import ProductGallery from "./ProductGallery";
import { DescriptionContainer } from "./DescriptionContainer";

export class BodyContainer extends React.Component {
	
	render(){
		return (
				<div id="mainbodycont">
					<ProductGallery />
					<DescriptionContainer />
				</div>
				
		       );
	}
}