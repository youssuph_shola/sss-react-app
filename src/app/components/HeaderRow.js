import React from "react";
import { render } from "react-dom";

import { CountryFlag } from "./CountryFlag";
import { SSSLogo } from "./SSSLogo";
import CartHeader from "./CartHeader";


export class HeaderRow extends React.Component {
	
	render(){
		return (
			<div id="logonav">
				<CountryFlag country="USA" />
				<SSSLogo />
				<CartHeader />			  
			</div>
		);
	}
}