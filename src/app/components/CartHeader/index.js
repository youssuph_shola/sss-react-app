import { connect } from 'react-redux'
import CartHeader from './CartHeader'

function mapStateToProps (state) {
  return {
    cart: state.cart,
  }
}

export default connect(mapStateToProps)(CartHeader)
