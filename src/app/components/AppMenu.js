import React from "react";

export class AppMenu extends React.Component {
	
	render(){
		return (  
		  <div id="navigationmenu">
			<ul>
			  <li><a href="#">HOME</a></li>
			  <li><a href="#">MEN</a></li>
			  <li><a href="#">WOMEN</a></li>
			  <li><a href="#">LOOKBOOK</a></li>
			  <li><a href="#">ABOUT</a></li>
			  <li><a href="#">BLOG</a></li>
			</ul>
		  </div>
		);
	}
}