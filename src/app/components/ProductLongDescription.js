//Header.js
import React from "react";

export class ProductLongDescription extends React.Component{
	
	render(){
		return (
			
			    <p className="long-description-container">
			    Sleek, polished, and boasting an impeccably modern fit, this blue, 2-but-
        ton Lazio suit features a notch lapel, flap pockets, and accompanying flat 
      front trousers—all in pure wool by Vitale Barberis Canonico.</p>
			
		);
	}
}