import React from "react";
import MainGalleryImage from "./MainGalleryImage";
import Thumbnails from "./Thumbnails";

export default class ProductGallery extends React.Component {
	
	render(){
		return (
				<div id="productgallery">
					<MainGalleryImage />
					<Thumbnails />
				</div>
		       );
	}
}