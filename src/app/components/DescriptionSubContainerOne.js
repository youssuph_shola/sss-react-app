//Header.js
import React from "react";
import { ProductDescriptionGroupHeader } from "./ProductDescriptionGroupHeader";


export class DescriptionSubContainerOne extends React.Component{
	
	render(){
		return (
			<div id = "DescText">
			    <ProductDescriptionGroupHeader />
			</div>
		);
	}
}