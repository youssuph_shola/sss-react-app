//Header.js
import React from "react";

export class ProductDescriptionAttributes extends React.Component{
	
	render(){
		return (
			<div>
			{this.props.data && 
				this.props.data.map((item, i) => (<div className="attriute-description" key={i}>
										              <span>{item.tag}</span>  : <span className="lighttext">{item.value}</span>
									             </div>))
			}
			</div>
		);
	}
}