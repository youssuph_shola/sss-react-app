import React from "react";
import {Button} from '../Button';
import CartServices from '../../Services/CartServices';

export default class AddToCartButton extends React.Component {
	

    addToCart = () => {

    	let formContent = this.props.formstatus;
    	if(formContent.quantity > 0){
	    	this.props.dispatch(CartServices.updateItem({color: formContent.color, 
						    							 size: formContent.size, 
						    							 quantity: formContent.quantity}));    		
    	}else{
    		alert('Quantity must be greater than 0')
    	}

    }

	render(){

		return (<Button name="addToCart"
			       className={(this.props.formstatus.cartButtonDisabled) ? "addtocart-disabled" : "addtocart-enabled"}
			       id="addToCartButton"
			       buttonLabel="  ADD TO CART  "
			       isDisabled={this.props.formstatus.cartButtonDisabled}
			       onClick = {()=>{this.addToCart()}} />)
	    
	}

}