import { connect } from 'react-redux'
import AddToCartButton from './AddToCartButton'

function mapStateToProps (state) {
  return {
    formstatus: state.formstatus,
  }
}

export default connect(mapStateToProps)(AddToCartButton)
