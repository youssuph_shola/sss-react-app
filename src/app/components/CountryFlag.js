import React from "react";
import { render } from "react-dom";
import {assetBaseUrl} from "../Constants/AssetConstants";


export class CountryFlag extends React.Component {
	
	render(){
		return (
			<div id="lang">
				<img src={`${assetBaseUrl}flags/${this.props.country}.jpg`} alt="" width="16" height="11" /> 
				{this.props.country}
			</div>
		);
	}
}