//Header.js
import React from "react";

export class ProductBulletedDescriptions extends React.Component{
	
	render(){
		return (
			<div className="bulleted-description-container">
			{this.props.data && 
				this.props.data.map((item, i) => (
					<div className="attriute-description" key={i}>
					 {item}
					</div>))
			}
			</div>
		);
	}
}