import { connect } from 'react-redux'
import Thumbnails from './Thumbnails'

function mapStateToProps (state) {
  return {
    gallery: state.gallery,
  }
}

export default connect(mapStateToProps)(Thumbnails)
