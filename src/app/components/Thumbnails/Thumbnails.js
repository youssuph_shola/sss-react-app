import React from "react";
import animateScrollTo from 'animated-scroll-to';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronRight, faChevronLeft } from '@fortawesome/fontawesome-free-solid';
import GalleryServices from "../../Services/GalleryServices";

export default class Thumbnails extends React.Component {
	
	
	constructor(props){
		super(props);

		this.state={
			currentScrollLength : 0
		};

		this.handleClick = this.handleClick.bind(this);
	}
	
	componentDidMount(){
		this.setState({scrollSize : this.getThumbnailWidth()});
	}

	handleClick(index) {

		this.props.dispatch(GalleryServices.displayLargeImage(index));

		let elems = document.getElementsByClassName('gallerythumbnail');

		for(let q=0; q < elems.length; q++){
			elems[q].style.opacity='1.0'
		}
		
		document.getElementById("thumb_"+index).style.opacity='0.5';
	}
	

	handleSCroll(direction) {

		let scrollValue = this.getScrollToValue(direction);

		animateScrollTo(scrollValue, 
						{element: document.querySelector('.ThumbnailsContainer'),
					     horizontal: true});

	}

    getThumbnailWidth(){
    	return document.getElementsByClassName('gallerythumbnail')[0].clientWidth;
    }

    getThumbnailContainerWidth(){
    	return document.getElementsByClassName('ThumbnailsContainer')[0].clientWidth;
    }
    
    getScrolledDistance() {
         return document.getElementsByClassName('ThumbnailsContainer')[0].scrollLeft;
    }

    getScrollToValue(direction){
    	const self = this;
    	switch(direction) {
    		case 'left':
    		    return self.getScrolledDistance() - self.getThumbnailWidth();
    		break;
    		case 'right':
    			return self.getScrolledDistance() + self.getThumbnailWidth();
    		break;
    	}
    }

	render(){
		
		var data = GalleryServices.getGalleryImages(),
			index = 0;
		
		return (
				<div className="scroll-container">
					<div className="scroller scroll-left" onClick={()=>this.handleSCroll('left')}>
						<FontAwesomeIcon icon="chevron-left" size="4x" />
					</div>				
			        <div className="ThumbnailsContainer">
					

						{data.map((item, i) => (


								<div className="gallerythumbnail" id={`thumb_${i}`}
													 key={i}>
													 
									<img className="ThumbnailImage" 
										 src={`${item.thumbnail}`} 
										 onClick={()=>this.handleClick(i)} />
										 
								</div>))
						}
						<div className="clear"></div>
						
					</div>
					<div className="scroller scroll-right" onClick={()=>this.handleSCroll('right')}>
							<FontAwesomeIcon icon="chevron-right"  size="4x" />
					</div>
				</div>
		       );
	}
}