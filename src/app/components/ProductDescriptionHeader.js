//Header.js
import React from "react";

export class ProductDescriptionHeader extends React.Component{
	
	render(){
		return (
			<p>
			    <span className="descheader">{this.props.description}</span>
			</p>
		);
	}
}