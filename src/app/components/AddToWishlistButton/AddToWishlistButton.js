import React from "react";
import {Button} from '../Button';
import WishlistServices from '../../Services/WishlistServices';


export default class AddToWishlistButton extends React.Component {



    addToWishlist = () => {

    	let formContent = this.props.formstatus;

    	this.props.dispatch(WishlistServices.addToWishList({color: formContent.color, 
					    									 size: formContent.size})); 

    	WishlistServices.updateWishlistStatus({size:formContent.size, 
    										   color:formContent.color});

    }

	render(){


	    return (<Button buttonName="addToWishlist"
	       className={(this.props.wishlist.isInWishlist) ? 'already-in-wishlist':"addtowish"}
	       id="addToWishlistButton"
	       buttonLabel="  ADD TO WISHLIST  "
	       disabled={this.props.formstatus.wishlistButtonDisabled}
	       onClick = {()=>{this.addToWishlist()}} />)
	}

}