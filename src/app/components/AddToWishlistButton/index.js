import { connect } from 'react-redux'
import AddToWishlistButton from './AddToWishlistButton'

function mapStateToProps (state) {
  return {
    formstatus: state.formstatus,
    wishlist: state.wishlist
  }
}

export default connect(mapStateToProps)(AddToWishlistButton)
