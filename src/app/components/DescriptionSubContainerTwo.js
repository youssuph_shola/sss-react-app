//Header.js
import React from "react";
import { ProductDescriptionAttributes } from "./ProductDescriptionAttributes";
import { ProductLongDescription } from "./ProductLongDescription";
import { ProductBulletedDescriptions } from "./ProductBulletedDescriptions";


export class DescriptionSubContainerTwo extends React.Component{
	
    getDescriptionTagData(){
        return [{tag:"Availability",
        		 value : "In stock"},
				{tag:"Product Code",
        		 value : "#4657"},
        		{tag:"Tags",
        		 value : "Fashion, Hood, Classic"},];
    }

    getBulletedDescriptionData(){
    	return ["•  Dark blue suit for a tone-on-tone look",
    		    "•  Regular fit",
    		    "•  100% Cotton", 
    		    "•  Free shipping with 4 days delivery"];
    }

	render(){

		return (
			<div className = "further-description">
			    <ProductDescriptionAttributes data={this.getDescriptionTagData()} />
			    <ProductLongDescription />
			    <ProductBulletedDescriptions data={this.getBulletedDescriptionData()} />
			</div>
		);
	}
}