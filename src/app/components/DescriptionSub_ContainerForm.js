//Header.js
import React from "react";
import { Dropdown } from "./Dropdown";


export class DescriptionSubContainerForm extends React.Component{

    getColorItems(){
        return [{value:'Red', label: 'Red'},
                {value:'Blue', label: 'Blue'},
                {value:'Green', label: 'Green'}];
    }

    getSizeItems(){
        return [{value:'S', label: 'S'},
                {value:'M', label: 'M'},
                {value:'L', label: 'L'},
                {value:'XL', label: 'XL'}]
    }


        getQuantityItems(){
        return [{value:1, label: 1},
                {value:2, label: 2},
                {value:3, label: 3},
                {value:4, label: 4},
                {value:5, label: 5},
                {value:6, label: 6},
                {value:7, label: 7},]
    }

	render(){

		return (<div className='form-container'>
    			    <Dropdown fieldLabel="COLOR"
                                 fieldName="colorField"
                                 fieldClassName = ""
                                 selectInstruction = "Select Color"
                                 items = {this.getColorItems()} />

                    <Dropdown fieldLabel="SIZE"
                                 fieldName="sizeField"
                                 fieldClassName = ""
                                 selectInstruction = "Select Size"
                                 items = {this.getColorItems()} />


                    <Dropdown fieldLabel="QUANTITY"
                                 fieldName="quantityField"
                                 fieldClassName = ""
                                 selectInstruction = "Select Quantity"
                                 items = {this.getQuantityItems()} />

                    <div className="clear"></div>
                </div>
		       );
	}
}