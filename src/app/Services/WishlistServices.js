import * as wishlistActions from '../redux/wishlist/actions';
import ProductServices from './ProductServices';
import store from '../redux';


class WishlistServices {

  constructor(){
    this.storageKey = 'WISHLIST_STORE';
  }

  static getWishlistLocalContent(){
    return store.getState().wishlist;
  }

  async getStorageWishlistContent(){
    let storageContent = await localStorage.getItem(this.storageKey);
    try{
      if(storageContent && JSON.parse(storageContent)){

        return JSON.parse(storageContent);
      }      
    }catch(e){
      console.log(e)
      return {};
    }
  }


  getNewWishlistContent(item){
    var wishlist = WishlistServices.getWishlistLocalContent();

    if(this.isInWishList(item)){
        return wishlist.items;
    }else{
        wishlist.items[item.sku] = true;
    }
    return wishlist.items;

  }


  updateWishlistItem(wishlistContent){

     return (dispatch) => {
        dispatch(wishlistActions.updateItem(wishlistContent));
        return true;
     }
  }

  updateLocalStorage(wishlistContent){
    localStorage.setItem(this.storageKey, JSON.stringify(wishlistContent))
  }
  
  addToWishList(item){

    var product = ProductServices.findProduct({color:item.color, size:item.size});

    if(product){
      item.sku = product.sku;
      let wishlistContent = this.getNewWishlistContent(item),
          allContent = {items:wishlistContent};

          this.updateLocalStorage(allContent);

          return this.updateWishlistItem(allContent);          
    }
  }

  isInWishList(item){
    var wishlist = WishlistServices.getWishlistLocalContent();

    if(item.sku===undefined){

      item = ProductServices.findProduct({color:item.color, size:item.size});
      if(!item){
        item = {};
      }
    }

    return (wishlist.items[item.sku] === true);
  }

  updateWishlistStatus(item){

    if(this.isInWishList(item)){
      store.dispatch(wishlistActions.setIsAdded(true));
    }else{
      store.dispatch(wishlistActions.setIsAdded(false));
    }
  }
}

export default new WishlistServices();