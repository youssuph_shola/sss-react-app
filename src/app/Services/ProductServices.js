import store from '../redux';
import _ from 'underscore';
import * as productActions from '../redux/products/actions';

export default class ProductServices {

  static setProducts(products){
     return (dispatch) => {
        dispatch(productActions.setProducts(products));
        return true;
     }  
  }

  static getProducts(){
    return store.getState().products.productItems; 
  }

  static findProduct(item){

    const products = ProductServices.getProducts(),
          product  = _.findWhere(products, {color: item.color, size: item.size});
          return product;

  }


  static findAllProducts(condition){

    const products = ProductServices.getProducts(),
          result  = _.where(products, condition);
          return result;

  }

  static isInStock(item){
    return item.in_stock;
  }

  static findBySku(){
    const products = ProductServices.getProducts(),
          product  = _.findWhere(products, {color: item.sku});
          return product;
  }

  static downloadProducts(){
    return fetch("http://localhost:8080/app/data/swatches.json")
           .then(res => res.json())
  }
}