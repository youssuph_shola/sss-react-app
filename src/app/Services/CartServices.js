import * as cartActions from '../redux/cart/actions';
import ProductServices from './ProductServices';
import store from '../redux';


class CartServices {

  constructor(){
    this.storageKey = 'PRODUCT_STORE';
  }

  static getCartLocalContent(){
    return store.getState().cart;
  }

  async getStorageCartContent(){
    let storageContent = await localStorage.getItem(this.storageKey);
    try{
      if(storageContent && JSON.parse(storageContent)){

        return JSON.parse(storageContent);
      }      
    }catch(e){
      console.log(e)
      return {};
    }

  }

  setCartLocalContent(){

  }

  setStorageCartContent(){

  }


  getNewCartContent(item){
    var cart = CartServices.getCartLocalContent();

    if(this.isInCart(item)){
       let content = cart.cartItems[item.sku];
          content.quantity = parseInt(content.quantity) + parseInt(item.quantity);
           cart.cartItems[item.sku] = content;
    }else{
           cart.cartItems[item.sku] = {quantity: parseInt(item.quantity)};
    }

    return cart.cartItems;

  }

  updateCartItem(cartContent){

     return (dispatch) => {
        dispatch(cartActions.updateItem(cartContent));
        return true;
     }    

  }

  updateLocalStorage(cartContent){
    localStorage.setItem(this.storageKey, JSON.stringify(cartContent))
  }
  
  updateItem(item){

    var product = ProductServices.findProduct({color:item.color, size:item.size});

    if(product && ProductServices.isInStock(product)){
      item.sku = product.sku;
      let cartContent = this.getNewCartContent(item),
          allContent = {cartItems:cartContent, items_count: this.countItems(cartContent)};

          this.updateLocalStorage(allContent);
        
          return this.updateCartItem(allContent);          
    }
  }

  countItems(cartContent){
    return Object.keys(cartContent).length;
  }


  isInCart(item){
    var cart = CartServices.getCartLocalContent();
    return cart.cartItems[item.sku] !== undefined;
  }


}

export default new CartServices();