import { gallery } from '../data/gallery'
import * as galleryActions from '../redux/gallery/actions'


export default class GalleryServices {

  static getGalleryImages = () =>{
	  return gallery.gallery.images;
  }

  static getGalleryImage = index =>{
	  let allImages = GalleryServices.getGalleryImages();
      return allImages[index];
  }
  
  static getThumbnail = index => {
	  let currentImage = GalleryServices.getGalleryImage(index);
	  return currentImage.thumbnail;
  }

  static getLargeImages = () => {
    let allImages = GalleryServices.getGalleryImages();
    return allImages.map(images => images.large);
  } 

  static getSmallImages = () => {
    let allImages = GalleryServices.getGalleryImages();
    return allImages.map(images => images.thumbnail);
  } 

  static getLargeImage = index => {
	  let currentImage = GalleryServices.getGalleryImage(index);
	  return currentImage.large;
  }

  static displayLargeImage = index => {

     return (dispatch) => {
        dispatch(galleryActions.setMainImage(index));
        return true;
     }	  
  }
}