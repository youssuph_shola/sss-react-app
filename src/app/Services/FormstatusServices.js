import * as formstatusActions from '../redux/formstatus/actions';
import ProductServices from './ProductServices';
import _ from 'underscore';


export default class FormstatusServices {

  static setColor = color =>{
     return (dispatch) => {
        dispatch(formstatusActions.setColor(color));
        return true;
     }
  }

  static setSize = size =>{
     return (dispatch) => {
        dispatch(formstatusActions.setSize(size));
        return true;
     }
  }
  
  static setQuantity = quantity => {
     return (dispatch) => {
        dispatch(formstatusActions.setQuantity(quantity));
        return true;
     }
  }
  
  static setAddToCartButtonStatus = content => {
     var status = !FormstatusServices.getAddToCartButtonStatus(content);
     return (dispatch) => {
        dispatch(formstatusActions.setButtonDisabled(status));
        return true;
     }
  }

  static getAddToCartButtonStatus = (content) => {

    let {color, size, quantity} = content;

    return color !== '0' &&
           size !== '0' &&
           quantity !== '0';
  }

  static resetFields = (alteredComponent, formValues, elements) =>{

    let colorObject = elements.color,
        sizeObject = elements.size;

    var otherObject = null;

     let condition = {[alteredComponent]: formValues[alteredComponent], in_stock: true};

     let products = ProductServices.findAllProducts(condition);

     let otherComponent = (alteredComponent === 'color') ? 'size' : 'color';     

     let availables = _(products).pluck(otherComponent);
   

    if(otherComponent === 'color')
      otherObject = colorObject;
    else if(otherComponent === 'size')
      otherObject = sizeObject;

    for(let d=1; d <otherObject.length; d++){
      otherObject[d].disabled = true;
    }

     _.each(availables, function(available){

        for(let d=1; d < otherObject.length; d++){

            if(available === otherObject[d].value){
                otherObject[d].disabled = false;
            }
        }

     })

  }
  

}