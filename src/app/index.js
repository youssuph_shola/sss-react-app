import React from "react";
import { render } from "react-dom";
import store from './redux'
import { Provider } from 'react-redux'
import { HeaderRow } from "./components/HeaderRow";
import { NavigationBar } from "./components/NavigationBar";
import { BodyContainer } from "./components/BodyContainer";
import ProductServices from "./Services/ProductServices";
import CartServices from "./Services/CartServices";
import WishlistServices from "./Services/WishlistServices";

class App extends React.Component {

	componentDidMount(){
		this._getData();
	}


  _getData = async () => {
    let products = await ProductServices.downloadProducts();
    let cartContent = await CartServices.getStorageCartContent();
    let wishlistContent = await WishlistServices.getStorageWishlistContent();
    await store.dispatch(ProductServices.setProducts(products)); 
    await store.dispatch(CartServices.updateCartItem(cartContent)); 
    await store.dispatch(WishlistServices.updateWishlistItem(wishlistContent));
  }	

	render(){
		return (
				<Provider store={store}>
					<div className = "container">
						<div className = "row">
							
						</div>
						
					</div>
					<HeaderRow />
					<NavigationBar />
					<BodyContainer />
				</Provider>
		);
	}
}

render(<App/>, window.document.getElementById("app"));